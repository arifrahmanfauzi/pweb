<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller extends CI_Controller {

	function __construct(){
        parent:: __construct();
        $this->load->model('M_login');

    }
	public function index()
	{
		$this->load->view('home');
	}
	function aksi_login(){
        $username = $this->input->post('username');
        $password = $this->input->post('password');
				$query = $this->db->get('user');
        $where = array(
            'username' => $username,
            'password' => $password
            );
        $cek = $this->M_login->cek_login("user",$where)->num_rows();
        if($cek > 0){
					$row = $query->row();
            $data_session = array(
                'nama' => $username,
								
								'surat' => $row->surah,
								'ayat' => $row->ayat,
                'status' => "login"
                );

            $this->session->set_userdata($data_session);

            redirect(base_url("Admin"));

        }else{
            echo "Username dan password salah !";
        }
    }

    function logout(){
        $this->session->sess_destroy();
        redirect(base_url('Controller'));
    }
}
